import cv2
import matplotlib.pyplot as plt

# List des 4 images choisies
image_paths = ['images-projet/IMG_0292.jpg', 'images-projet/IMG_0576.jpg', 'images-projet/IMG_0884.jpg', 'images-projet/IMG_0225.jpg']

# Boucle pour parcourir chaque image et l'afficher sous chaque espace
for i, img_path in enumerate(image_paths, start=1):
    # Lecture
    bgr_img = cv2.imread(img_path)

    # Conversion de l'image en rgb vers hsv
    hsv_img = cv2.cvtColor(bgr_img, cv2.COLOR_BGR2HSV)

    # On recupere les differents channels dans l'espace hsv
    h, s, v = cv2.split(hsv_img)

    # On recupere les differents channels dans l'espace
    b, g, r = cv2.split(bgr_img)

    # Affichage des images
    plt.figure(figsize=(16, 8))

    # Affichage de l'image en RGB
    plt.subplot(2, 4, 1)
    plt.title('RGB Image')
    plt.imshow(cv2.cvtColor(bgr_img, cv2.COLOR_BGR2RGB))
    plt.axis('off')

    # Affichage de l'image sous le channel Rouge
    plt.subplot(2, 4, 2)
    plt.title('Red (R)')
    plt.imshow(r, cmap='gray')
    plt.axis('off')

    # Affichage de l'image sous le channel Vert
    plt.subplot(2, 4, 3)
    plt.title('Green (G)')
    plt.imshow(g, cmap='gray')
    plt.axis('off')

    # Affichage de l'image sous le channel Bleue
    plt.subplot(2, 4, 4)
    plt.title('Blue (B)')
    plt.imshow(b, cmap='gray')
    plt.axis('off')

    # Affichage de l'image en HSV
    plt.subplot(2, 4, 5)
    plt.title('HSV Image')
    plt.imshow(cv2.cvtColor(hsv_img, cv2.COLOR_BGR2RGB))
    plt.axis('off')

    # Affichage de l'image sous le channel Hue
    plt.subplot(2, 4, 6)
    plt.title('Hue (H)')
    plt.imshow(h, cmap='gray')
    plt.axis('off')

    # Affichage de l'image sous le channel Saturation
    plt.subplot(2, 4, 7)
    plt.title('Saturation (S)')
    plt.imshow(s, cmap='gray')
    plt.axis('off')

    # Affichage de l'image sous le channel Value
    plt.subplot(2, 4, 8)
    plt.title('Value (V)')
    plt.imshow(v, cmap='gray')
    plt.axis('off')

    plt.tight_layout()
    plt.show()
