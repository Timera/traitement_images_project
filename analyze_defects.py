import cv2
import os
def analyze_defects(image_path):

    image_name = os.path.basename(image_path)
    print("Nom de l'image :", image_name)

    image_ori = cv2.imread(image_path)
    image_traitee = cv2.imread(image_path)

    # Convertir l'image en niveaux de gris
    gray = cv2.cvtColor(image_traitee, cv2.COLOR_BGR2GRAY)

    # Utilisation du flou gaussien pour réduire le bruit
    blurred = cv2.GaussianBlur(gray, (5, 5), 0)

    # Canny pour détecter les contours
    edges = cv2.Canny(blurred, 50, 160)

    # Les contours dans l'image
    contours, _ = cv2.findContours(edges, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # Parcourir les contours détectés
    for i, contour in enumerate(contours):
        # Calcul de l'aire du contour
        area = cv2.contourArea(contour)

        # Définition d'une taille minimale d'aire pour filtrer les contours indésirables
        min_area = 100
        if area < min_area:
            # Rectangle englobant pour chaque contour
            x, y, w, h = cv2.boundingRect(contour)

            # Dessiner un  rouge autour du contour
            cv2.rectangle(gray, (x, y), (x + w, y + h), (0, 0, 255), 2)
            perimeter = cv2.arcLength(contour, True)
            aspect_ratio = float(w) / h
            print("Zone de défaut {}:".format(i + 1))
            print("- Surface : {:.2f}".format(area))
            print("- Périmètre : {:.2f}".format(perimeter))
            print("- Ratio hauteur/largeur : {:.2f}".format(aspect_ratio))
            print("- Position (x, y) : ({}, {})".format(x, y))
            print("- Taille (largeur, hauteur) : ({}, {})".format(w, h))
            print()

    # Afficher l'image avec les contours sur les zones de défauts
    cv2.imshow('Contours encadres', gray)
    cv2.imshow("Image Originale", image_ori)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


image_path = "images-projet/IMG_0884.jpg"

# Appel de la fonction pour analyser les défauts sur l'image
analyze_defects(image_path)

