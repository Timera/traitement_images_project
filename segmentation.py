import numpy as np
import cv2

image = cv2.imread('images-projet/IMG_0225.jpg')

# Créer l'espace de couleur HSV
hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

# Définir les limites de couleur inférieure et supérieure
low_val = (0, 60, 0)
high_val = (179, 255, 255)

# Seuiller l'image HSV
mask = cv2.inRange(hsv, low_val, high_val)

# Supprimer le bruit
mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel=np.ones((8, 8), dtype=np.uint8))

# Appliquer le masque à l'image d'origine
result = cv2.bitwise_and(image, image, mask=mask)

#Affiche de l'image segmentée et de l'image originale
cv2.imshow("Image Segmentee", result)
cv2.imshow("Image Originale", image)

cv2.waitKey(0)
cv2.destroyAllWindows()