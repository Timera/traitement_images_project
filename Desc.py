# on importe les librairies nécessaires
import cv2
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt

# Chargement de l'image chosie
nom_image = 'images-projet/IMG_0579.jpg'
image = Image.open(nom_image)

# Affichage de l'histogramme de niveaux de gris
plt.figure(figsize=(10, 5))
plt.hist(np.array(image).ravel(), bins=256, range=[0, 256], color='gray')
plt.title("Histogramme de niveaux de gris")
plt.xlabel("Niveaux de gris")
plt.ylabel("Fréquence")
plt.show()

# Histogramme en niveaux de couleurs
# Conversion de l'image PIL en tableau NumPy pour pouvoir appliquer la fonction cvtColor
image_np = np.array(image)

# Conversion de l'image en mode RGB avec OpenCV
image_rgb = cv2.cvtColor(image_np, cv2.COLOR_BGR2RGB)

# recuperaction des niveaux de rouge, bleu et vert
r, g, b = cv2.split(image_rgb)
histr = cv2.calcHist([r], [0], None, [255], [0, 255])
histg = cv2.calcHist([g], [0], None, [255], [0, 255])
histb = cv2.calcHist([b], [0], None, [255], [0, 255])

# Tracage de l'histogramme
plt.plot(histr, color='r', label='Rouge')
plt.plot(histg, color='g', label='Vert')
plt.plot(histb, color='b', label='Bleu')

plt.xlim([0, 255])
plt.legend()
plt.show()

# Analyse des autres detailes de l'image

couleurs = image.getcolors()
mode_image = image.mode
format_image = image.format
largeur_image = image.width
hauteur_image = image.height
taille_image = image.size
palette_image = image.palette

# Affichage de ces resultats
print("Couleurs dominantes:", couleurs)
print("Palete de l'image :", palette_image)
print("Mode de l'image:", mode_image)
print("Format de l'image :", format_image)
print("Taille de l'image :", taille_image)
print("Hauteur de l'image :", hauteur_image)
print("Largeur de l'image :", largeur_image)
